// console.log(window.location);
if (window.location.search === "?mode=adverts") {
  let findOffer = new Offer();

  findOffer.start();
}

if (window.location.pathname === "/users_player.php") {
  // let userPlayer = new User();
  //
  // userPlayer.start();
}

if (window.location.pathname === "/transfers.php" && (!window.location.search || window.location.search.indexOf('page') !== -1) && chrome.storage) {
  chrome.storage.sync.get(['fpSearch'], function (result) {
    if (result.fpSearch) {
      let findTransfer = new Transfers();

      findTransfer.init();
    }
  });
}

function onlyUnique(value, index, self) {
  return self.indexOf(value) === index;
}