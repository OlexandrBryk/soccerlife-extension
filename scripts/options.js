'use strict';

let fpSearch = document.getElementById('fpSearch');
let pTodayAll = document.getElementById('pTodayAll');
let pToday = document.getElementById('pToday');
let pTomorrow = document.getElementById('pTomorrow');
let pCountry = document.getElementById('pCountry');
let pPosition = document.getElementById('pPosition');
let pAgeMin = document.getElementById('pAgeMin');
let pAgeMax = document.getElementById('pAgeMax');
let pTalMin = document.getElementById('pTalMin');
let pTalMax = document.getElementById('pTalMax');
let pSkillMin = document.getElementById('pSkillMin');
let pSkillMax = document.getElementById('pSkillMax');
let pPriceMin = document.getElementById('pPriceMin');
let pPriceMax = document.getElementById('pPriceMax');

/** Load Settings */
chrome.storage.sync.get(['fpSearch'], function (result) {
  fpSearch.checked = result.fpSearch;
});


chrome.storage.sync.get(['pTodayAll'], function (result) {
  pTodayAll.checked = result.pTodayAll;
});

chrome.storage.sync.get(['pToday'], function (result) {
  pToday.checked = result.pToday;
});

chrome.storage.sync.get(['pTomorrow'], function (result) {
  pTomorrow.checked = result.pTomorrow;
});


chrome.storage.sync.get(['pCountry'], function (result) {
  countries.forEach((value, key) => {
    let option = new Option(value, key);
    pCountry.options.add(option);
  });

  if (result.pCountry) {
    pCountry.value = result.pCountry.value;
  }
});

chrome.storage.sync.get(['pPosition'], function (result) {
  positions.forEach((value, key) => {
    let option = new Option(value, key);
    pPosition.options.add(option);
  });

  pPosition.value = result.pPosition;
});


chrome.storage.sync.get(['pAgeMin'], function (result) {
  pAgeMin.value = result.pAgeMin ? result.pAgeMin : ageMin;
});

chrome.storage.sync.get(['pAgeMax'], function (result) {
  pAgeMax.value = result.pAgeMax ? result.pAgeMax : ageMax;
});


chrome.storage.sync.get(['pTalMin'], function (result) {
  pTalMin.value = result.pTalMin ? result.pTalMin : talMin;
});

chrome.storage.sync.get(['pTalMax'], function (result) {
  pTalMax.value = result.pTalMax ? result.pTalMax : talMax;
});


chrome.storage.sync.get(['pSkillMin'], function (result) {
  pSkillMin.value = result.pSkillMin ? result.pSkillMin : skillMin;
});

chrome.storage.sync.get(['pSkillMax'], function (result) {
  pSkillMax.value = result.pSkillMax ? result.pSkillMax : skillMax;
});


chrome.storage.sync.get(['pPriceMin'], function (result) {
  pPriceMin.value = result.pPriceMin ? result.pPriceMin : priceMin;
});

chrome.storage.sync.get(['pPriceMax'], function (result) {
  pPriceMax.value = result.pPriceMax ? result.pPriceMax : priceMax;
});

/** Save Settings */
fpSearch.addEventListener('change', (event) => {
  chrome.storage.sync.set({fpSearch: event.target.checked}, function () {
  })
});


pTodayAll.addEventListener('change', (event) => {
  chrome.storage.sync.set({pTodayAll: event.target.checked}, function () {
  })
});

pToday.addEventListener('change', (event) => {
  chrome.storage.sync.set({pToday: event.target.checked}, function () {
  });

  if (event.target.checked) {
    chrome.storage.sync.set({pTomorrow: false}, function () {
    });
  }

  pTomorrow.checked = event.target.checked ? false : pTomorrow.checked;
});

pTomorrow.addEventListener('change', (event) => {
  if (event.target.checked) {
    chrome.storage.sync.set({pToday: false}, function () {
    });
  }

  chrome.storage.sync.set({pTomorrow: event.target.checked}, function () {
  });

  pToday.checked = event.target.checked ? false : pToday.checked;
});


pCountry.addEventListener('change', () => {
  chrome.storage.sync.set({pCountry: {value: pCountry.value, title: pCountry.options[pCountry.selectedIndex].innerText}}, function () {
  })
});

pPosition.addEventListener('change', () => {
  chrome.storage.sync.set({pPosition: pPosition.value}, function () {
  })
});


pAgeMin.addEventListener('change', () => {
  chrome.storage.sync.set({pAgeMin: pAgeMin.value}, function () {
  })
});

pAgeMax.addEventListener('change', () => {
  chrome.storage.sync.set({pAgeMax: pAgeMax.value}, function () {
  })
});


pTalMin.addEventListener('change', () => {
  chrome.storage.sync.set({pTalMin: pTalMin.value}, function () {
  })
});

pTalMax.addEventListener('change', () => {
  chrome.storage.sync.set({pTalMax: pTalMax.value}, function () {
  })
});


pSkillMin.addEventListener('change', () => {
  chrome.storage.sync.set({pSkillMin: pSkillMin.value}, function () {
  })
});

pSkillMax.addEventListener('change', () => {
  chrome.storage.sync.set({pSkillMax: pSkillMax.value}, function () {
  })
});


pPriceMin.addEventListener('change', () => {
  chrome.storage.sync.set({pPriceMin: pPriceMin.value}, function () {
  })
});

pPriceMax.addEventListener('change', () => {
  chrome.storage.sync.set({pPriceMax: pPriceMax.value}, function () {
  })
});

// let page = document.getElementById('buttonDiv');
// const kButtonColors = ['#3aa757', '#e8453c', '#f9bb2d', '#4688f1', '#fff'];
//
// function constructOptions(kButtonColors) {
//   for (let item of kButtonColors) {
//     let button = document.createElement('button');
//     button.style.backgroundColor = item;
//     button.addEventListener('click', function () {
//       chrome.storage.sync.set({color: item}, function () {
//         console.log('color is ' + item);
//       })
//     });
//     page.appendChild(button);
//   }
// }
//
// constructOptions(kButtonColors);
