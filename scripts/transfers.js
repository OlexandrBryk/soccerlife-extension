﻿'use strict';

class Transfers {
  constructor() {
    this.playersAll = [];
    this.playerTodayAll = false;
    this.playerToday = false;
    this.playerTomorrow = false;
    this.playerCountry = '';
    this.playerPosition = '';
    this.playerAgeMin = '';
    this.playerAgeMax = '';
    this.playerTalMin = '';
    this.playerTalMax = '';
    this.playerSkillMin = '';
    this.playerSkillMax = '';
    this.playerPriceMin = '';
    this.playerPriceMax = '';
  }

  init() {
    let that = this;
    chrome.storage.sync.get(null, function (result) {
      chrome.storage.local.get(['playersAll'], function (resultLocal) {
        that.playersAll = resultLocal.playersAll ? resultLocal.playersAll : that.playersAll;

        that.playerTodayAll = result.pTodayAll ? result.pTodayAll : false;
        that.playerToday = result.pToday ? result.pToday : false;
        that.playerTomorrow = result.pTomorrow ? result.pTomorrow : false;
        that.playerCountry = result.pCountry.value ? `${result.pCountry.title} (${result.pCountry.value})` : false;
        that.playerPosition = result.pPosition;
        that.playerAgeMin = result.pAgeMin ? result.pAgeMin : ageMin;
        that.playerAgeMax = result.pAgeMax ? result.pAgeMax : ageMax;
        that.playerTalMin = result.pTalMin ? result.pTalMin : talMin;
        that.playerTalMax = result.pTalMax ? result.pTalMax : talMax;
        that.playerSkillMin = result.pSkillMin ? result.pSkillMin : skillMin;
        that.playerSkillMax = result.pSkillMax ? result.pSkillMax : skillMax;
        that.playerPriceMin = result.pPriceMin ? result.pPriceMin : priceMin;
        that.playerPriceMax = result.pPriceMax ? result.pPriceMax : priceMax;

        that.find();
      });
    });
  }

  find() {
    let pageNum = 0;
    let findPrice = false;
    let query = window.location.search.substring(1);

    if (query) {
      let vars = query.split('&');
      let last = vars[vars.length - 1];
      let lastPair = last.split('=');

      pageNum = lastPair[1];
    }

    let cnt = 0;
    let tr = document.getElementsByClassName("trans_market_offers")[0].getElementsByTagName("tr");

    if (pageNum >= 0) {
      for (let i = 0; i < tr.length; i++) {
        let td = tr[i].getElementsByTagName("td");

        if (td[1] && td[2] && td[4] && td[5] && td[6] && td[8]) {
          // let div = td[2].getElementsByTagName("span");
          let flags = td[2].getElementsByClassName("flags");
          //let dateSpans = td[8].getElementsByTagName("span");
          let dateSpans = td[8].getElementsByClassName("wide_format");
          let dateSpansIn = dateSpans[0].getElementsByTagName("span");

          let isCountryEqual = false;

          if (flags.length) {
            for (let i = 0; i < flags.length; i++) {
              if (flags[i].title === this.playerCountry) isCountryEqual = true;
            }
          }

          let isTodayAll = this.playerTodayAll && dateSpansIn.length && dateSpansIn[0].classList.contains('yesterday');

          let isPositionD = this.playerPosition !== 'd' || ((td[1].textContent.indexOf('cd') !== -1) || (td[1].textContent.indexOf('rd') !== -1) || (td[1].textContent.indexOf('ld') !== -1));
          let isPosition = !this.playerPosition || ((td[1].textContent.indexOf(this.playerPosition) !== -1) && isPositionD);
          let isCountry = !this.playerCountry || isCountryEqual;
          let isToday = !this.playerToday || (dateSpansIn.length && dateSpansIn[0].classList.contains('yesterday'));
          let isTomorrow = !this.playerTomorrow || (dateSpansIn.length && !dateSpansIn[0].classList.contains('yesterday') && (dateSpansIn[0].textContent.indexOf('Завтра') !== -1));

          let isAge = +this.playerAgeMin <= td[4].textContent && +this.playerAgeMax >= td[4].textContent;
          let isTal = +this.playerTalMin <= td[5].textContent && +this.playerTalMax >= td[5].textContent;
          let isSkill = +this.playerSkillMin <= td[6].textContent && +this.playerSkillMax >= td[6].textContent;

          let price = +td[7].textContent.trim().replace('M', '');
          let isPrice = +this.playerPriceMin <= price && +this.playerPriceMax >= price;

          if (isPrice) findPrice = true;

          if (isTodayAll || (!this.playerTodayAll && isPosition && isCountry && isToday && isTomorrow && isAge && isTal && isSkill && isPrice)) {
            // this.playersAll.push(td[2].getElementsByTagName("a")[0].textContent);
            this.playersAll.push(tr[i].outerHTML);

            cnt++;

            td[2].style.backgroundColor = "red";
          }
        }
      }
    }

    if (cnt > 0) {
      let uniquePlayers = this.playersAll.filter(onlyUnique);

      this.playersAll = uniquePlayers;

      chrome.storage.local.set({playersAll: uniquePlayers}, function () {
        let error = chrome.runtime.lastError;

        if (error) {
          console.log(error);
          alert(error);
        }
      })
    }

    if (pageNum >= 0) {
      setTimeout(function () {
        if (findPrice) {
          document.location.search = '?page=' + --pageNum;
        } else {
          document.location.search = '?page=-1';
        }
      }, 2000);
    }

    if (pageNum < 0) {
      let newHTML = '';
      // let tableBody = document.getElementById("mylogs").nextSibling.nextSibling.tBodies[0];
      let tableBody = document.getElementsByClassName("trans_market_offers")[0].tBodies[0];

      let uniquePlayers = this.playersAll.filter(value => value.toString() !== '[object Object]');

      let uniquePlayersID = [];
      let uniquePlayersNew = [];

      for (let i = uniquePlayers.length - 1; i >= 0; i--) {
        let thisIdIndex = uniquePlayers[i].indexOf('id="tl-');
        let thisID = uniquePlayers[i].substr(thisIdIndex + 8, 7);

        if (!uniquePlayersID.includes(thisID)) {
          uniquePlayersNew.unshift(uniquePlayers[i]);
          uniquePlayersID.push(thisID);
        }
      }

      // for (let i = 0; i < this.playersAll.length; i++) {
      //   newHTML += this.playersAll[i];
      // }

      for (let i = 0; i < uniquePlayersNew.length; i++) {
        newHTML += uniquePlayersNew[i];
      }

      // tableBody.innerHTML = tableBody.innerHTML + newHTML;
      tableBody.innerHTML = newHTML;
    }
  }
}