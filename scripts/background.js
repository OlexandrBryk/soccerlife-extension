chrome.runtime.onInstalled.addListener(function () {
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function () {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [
        new chrome.declarativeContent.PageStateMatcher({
          pageUrl: {hostEquals: 'soccerlife.ru'},
        })
      ],
      actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });

  chrome.runtime.onMessage.addListener(
    function (request, sender, sendResponse) {
      if (request.showNotification) {
        let opt = {
          type: 'basic',
          title: request.title,
          message: request.msg,
          priority: 1,
          iconUrl: '../images/icon.png'
        };

        chrome.notifications.create('', opt, function (notificationId) {
          // console.log("Last error:");
          // console.log(chrome.runtime);
        });
        sendResponse("Notification shown.");
      }
    });
});