'use strict';

// let changeColor = document.getElementById('changeColor');
let offerMinVal = document.getElementById('offerMinVal');
let clearSearch = document.getElementById('clearSearch');
let playerTrain = document.getElementById('playerTrain');
let playerTrainAuto = document.getElementById('playerTrainAuto');

chrome.storage.local.get(['offerMin'], function (result) {
  offerMinVal.innerHTML = result.offerMin;
});

chrome.storage.sync.get(['playerTrain'], function (result) {
  const trainsNew = new Map([
    ["1", "Исторические"],
    ["2", "Разминка"],
    ["3", "Воркаут"],
    ["4", "Развитие"],
    ["5", "Релакс"]
  ]);

  trainsNew.forEach((value, key) => {
    let option = new Option(value, key);

    playerTrain.options.add(option);
  });

  if (result.playerTrain) {
    playerTrain.value = result.playerTrain.value;
  }
});

chrome.storage.sync.get(['playerTrainAuto'], function (result) {
  playerTrainAuto.checked = result.playerTrainAuto;
});

clearSearch.addEventListener('click', () => {
  chrome.storage.local.set({playersAll: []}, function () {
  });

  chrome.tabs.getSelected(null, function (tab) {
    var code = 'window.location.reload();';
    chrome.tabs.executeScript(tab.id, {code: code});
  });

  return false;
});

playerTrain.addEventListener('change', () => {
  chrome.storage.sync.set({playerTrain: {value: playerTrain.value, title: playerTrain.options[playerTrain.selectedIndex].innerText}}, function () {
  })
});

playerTrainAuto.addEventListener('change', (event) => {
  chrome.storage.sync.set({playerTrainAuto: event.target.checked}, function () {
  })
});

// Get All
// chrome.storage.local.get(null, function (result) {
//   alert(JSON.stringify(result));
// });
//
// chrome.storage.sync.get(null, function (result) {
//   alert(JSON.stringify(result));
// });

// Clear Storage
// chrome.storage.local.clear();
// chrome.storage.sync.clear();

// chrome.storage.sync.get('color', function (data) {
//   changeColor.style.backgroundColor = data.color;
//   changeColor.setAttribute('value', data.color);
// });
//
// changeColor.onclick = function (element) {
//   let color = element.target.value;
//   chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
//     chrome.tabs.executeScript(
//       tabs[0].id,
//       {code: 'document.body.style.backgroundColor = "' + color + '";'});
//   });
// };