'use strict';

class Offer {
  constructor() {
    this._secconds = advertsTime;

    this.interval = "";
    this.offerMin = offerMinValue;
    this.offerMinButton = "";
    // this.runInterval = "";
    // this.count = 0;
  }

  setMin(value) {
    this.offerMin = value;

    console.log(value);
    console.log({offerMin: value});

    chrome.storage.local.set({offerMin: value}, function () {
      // console.log('Value of offerMin set to ' + value);
    });
  };

  findMin() {
    let acceptedOffers = document.querySelectorAll(".billboard_gain span");
    let min = offerMaxValue;
    for (let i = 0; i < acceptedOffers.length; i++) {
      let currentValue = +acceptedOffers[i].innerText.replace("'", "");
      if (currentValue < min) {
        min = currentValue;
        switch (acceptedOffers[i].parentElement.parentElement.id) {
          case "billboard_cost_right":
            this.offerMinButton = "billboard_arrow_right";
            break;
          case "billboard_cost_left":
            this.offerMinButton = "billboard_arrow_left";
            break;
          default:
            if (acceptedOffers[i].parentElement.nextElementSibling && acceptedOffers[i].parentElement.nextElementSibling.id === "billboard_slot_top") {
              this.offerMinButton = "billboard_arrow_up";
            } else {
              this.offerMinButton = "billboard_arrow_down";
            }
        }
      }
    }

    console.log(min);
    return min;
  };

  start() {
    this.dif();
    let that = this;
    this.interval = setInterval(function () {
      that.dif.apply(that);
    }, this._secconds * 1000);
  };

  dif() {
    // clearInterval(this.runInterval);

    this.setMin(this.findMin());

    // this.count = this._secconds;
    // console.log("%s:%s", parseInt(this.count / 60), this.count % 60 ? this.count % 60 : "00");
    // let that = this;
    // this.runInterval = setInterval(function () {
    //   that.count -= 60;
    // console.log("%s:%s", parseInt(that.count / 60), that.count % 60 ? that.count % 60 : "00");
    // }, 60000);

    let offerString,
      wrap = document.getElementById("billboard_offer_cost");
    if (wrap.getElementsByTagName("b")[0]) {
      offerString = wrap.getElementsByTagName("b")[0].innerText;
    } else {
      console.log('%c %s ', logAcceptedCSS, logAcceptedMSG);
      this.sendNotification(logAcceptedMSG, logAcceptedTitle);
      return;
    }

    let offer = parseInt(offerString);
    if (this.offerMin < offer * 1000) {
      console.log('%c %s %s ', logOfferCSS, logOfferMSG, offerString);
      this.sendNotification(logOfferMSG + " " + offerString, logOfferTitle);
      document.getElementById(this.offerMinButton).click();
    } else {
      console.log("%c %d !< %d ", logSimpleOfferCSS, this.offerMin, offer * 1000);
      this.sendNotification(this.offerMin + " !< " + offer * 1000, logSimpleOfferTitle);
    }
  };

  sendNotification(msg, title) {
    let data = {
      showNotification: true,
      msg: "" + msg,
      title: "" + title
    };

    chrome.runtime.sendMessage(data, function (response) {
      if (response) {
        console.log('Notification Response: ' + response);
      }
    });
  }

  stop() {
    clearInterval(this.interval);
    // clearInterval(this.runInterval);
  };

  restart() {
    this.stop();
    this.start();
  }
}